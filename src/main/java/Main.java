import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ListUtils utils = new ListUtils();
        List<String> words = new ArrayList<>();
        words.add("Anna");
        words.add("Victor");
        words.add("Helen");
        words.add("Rita");
        words.add("bird");
        words.add("fox");
        words.add("cat");
        words.add("Nastya");
        words.add("Victor");
        words.add("Helen");
        words.add("Helen");
        words.add("Helen");
        words.add("Helen");
        words.add("Helen");
        words.add("Helen");

        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            numbers.add(1);
            numbers.add(2);
            numbers.add(3);
        }
        numbers.add(5);
        int[] mas = new int[]{1, 2, 3, 4};
        System.out.printf("%s %d", "This word is repeated:", utils.countOccurance(words, "Helen"));
        System.out.printf("%s %s", "To list:", utils.toList(mas).toString());
        System.out.printf("%s %s", "Unique:", utils.findUnique(numbers).toString());

        utils.calcOccurance(words);
        List listWords = utils.findOccurance(words);
        System.out.println(listWords.toString());
    }
}
