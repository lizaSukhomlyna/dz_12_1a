import java.util.Objects;

public class Word {
    private String name;
    private int occurrence;

    public Word(String name, int occurrence) {
        this.name = name;
        this.occurrence = occurrence;
    }

    public String getName() {
        return name;
    }

    public int getOccurrence() {
        return occurrence;
    }

    @Override
    public String toString() {
        return "{" +
                "name:'" + name + '\'' +
                ", occurrence:" + occurrence +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return occurrence == word.occurrence &&
                Objects.equals(name, word.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, occurrence);
    }
}
