import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListUtils {

    public int countOccurance(List<String> listOfStrings, String wordForSearch) {
        int countWord = 0;
        for (String wordFromList : listOfStrings) {
            if (wordFromList.equals(wordForSearch)) {
                countWord++;
            }
        }
        return countWord;
    }

    public List toList(int[] mas) {
        return Arrays.asList(mas);
    }

    public List findUnique(List<Integer> listOfInteger) {
        List<Integer> listOfUniqueInteger = new ArrayList<>();
        for (int i = 0; i < listOfInteger.size(); i++) {
            int countNum = 0;
            for (int j : listOfInteger) {
                if (listOfInteger.get(i) == j) {
                    countNum++;
                }
            }
            if (countNum == 1) {
                listOfUniqueInteger.add(listOfInteger.get(i));
            }
        }
        return listOfUniqueInteger;
    }

    public void calcOccurance(List<String> listOfString) {
        List<Word> listOfWordsAndThemOccur = new ArrayList<>();
        for (int i = 0; i < listOfString.size(); i++) {
            int countNum = 0;
            for (String j : listOfString) {
                if (listOfString.get(i).equals(j)) {
                    countNum++;
                }
            }
            Word wordForAdd = new Word(listOfString.get(i), countNum);
            if (!listOfWordsAndThemOccur.contains(wordForAdd))
                listOfWordsAndThemOccur.add(wordForAdd);
        }
        for (Word word : listOfWordsAndThemOccur) {
            System.out.printf("%s:%d \n", word.getName(), word.getOccurrence());
        }
    }

    public List findOccurance(List<String> listOfString) {
        List<Word> listOfWordsAndThemOccur = new ArrayList<>();
        for (int i = 0; i < listOfString.size(); i++) {
            int countNum = 0;
            for (String j : listOfString) {
                if (listOfString.get(i).equals(j)) {
                    countNum++;
                }
            }
            Word wordForAdd = new Word(listOfString.get(i), countNum);
            if (!listOfWordsAndThemOccur.contains(wordForAdd))
                listOfWordsAndThemOccur.add(wordForAdd);
        }
        return listOfWordsAndThemOccur;
    }
}
